/**
 * 
 */
package kmeansImp;

/**
 * @author Doga
 *
 */

/*
Program for K-Means Clustering using Postgres Database in Java
Author: Manav Sanghavi	Author Link: https://www.facebook.com/manav.sanghavi
www.pracspedia.com

To run the file:
javac -classpath postgresql.jar KMeansDatabase.java
java -classpath postgresql.jar KMeansDatabase
*/
import java.util.Random;
import java.sql.*;
import java.sql.Statement;

class generateS {

public static void main(String args[]) {
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	int NR = 6;
	int NS=10;
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	
	Connection con = null;
	try {

		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	//INSERT INTO public.r(rid, x) VALUES (?, ?);
	Statement stmt = null;
	int j=4;
	int[] items = new int[]{1,2,3};
	int[] items2 = new int[] {100,200,300};
	int[] fkVals1 = new int[NR/2];
	int[] fkVals2 = new int[NR/2];
	for(int i=0; i<fkVals1.length; i++)
	    fkVals1[i] = i+1;
	for(int i=0; i<fkVals2.length; i++)
	    fkVals2[i] = i+1+NR/2;
	Random rand = new Random();
	int fk=1;
	String sqld = "DELETE FROM S";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sqld);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	for (int i=1; i<=NS; i++){
	if(i<=NS/2){
    j = items[rand.nextInt(items.length)];
    fk = fkVals1[rand.nextInt(fkVals1.length)];
	}
	else {
    j = items2[rand.nextInt(items.length)];
    fk = fkVals2[rand.nextInt(fkVals2.length)];
	}
	String sql = "INSERT INTO S(sid, fk, y, z) VALUES ("+i+","+fk+","+j+","+j+")";
	//String query = "SELECT RID, X FROM R";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sql);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	}
	System.out.println("Done");
}
}

