/**
 * 
 */
package kmeansImp;

/**
 * @author Doga
 *
 */

/*
Program for K-Means Clustering using Postgres Database in Java
Author: Manav Sanghavi	Author Link: https://www.facebook.com/manav.sanghavi
www.pracspedia.com

To run the file:
javac -classpath postgresql.jar KMeansDatabase.java
java -classpath postgresql.jar KMeansDatabase
*/
import java.util.Random;
import java.sql.*;
import java.sql.Statement;

class GenerateTableS {

public static void main(String args[]) {
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	int NR = 6;
	int NS=10;
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	
	Connection con = null;
	try {

		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	//INSERT INTO public.r(rid, x) VALUES (?, ?);
	Statement stmt = null;
	int j1=4;
	int j2=2;
	int j3=5;
	int j4=1;
	int j5=3;
	int j6=7;
	int j7=6;
	int[] items = new int[]{1,2,3,4,5,6,7,8};
	int[] items2 = new int[]{100,200,300,400,500,600};
	int[] fkVals1 = new int[NR/2];
	int[] fkVals2 = new int[NR/2];
	for(int i=0; i<fkVals1.length; i++)
	    fkVals1[i] = i+1;
	for(int i=0; i<fkVals2.length; i++)
	    fkVals2[i] = i+1+NR/2;
	Random rand = new Random();
	int fk=1;
	String sqld = "DELETE FROM sf";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sqld);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	for (int i=1; i<=NS; i++){
	if(i<=NS/2){
    j1 = items[rand.nextInt(items.length)];
    j2 = items[rand.nextInt(items.length)];
    j3 = items[rand.nextInt(items.length)];
    j4 = items[rand.nextInt(items.length)];
    j5 = items[rand.nextInt(items.length)];
    j6 = items[rand.nextInt(items.length)];
    j7 = items[rand.nextInt(items.length)];
    fk = fkVals1[rand.nextInt(fkVals1.length)];
	}
	else {
    j1 = items2[rand.nextInt(items2.length)];
    j2 = items2[rand.nextInt(items2.length)];
    j3 = items2[rand.nextInt(items2.length)];
    j4 = items2[rand.nextInt(items2.length)];
    j5 = items2[rand.nextInt(items2.length)];
    j6 = items2[rand.nextInt(items2.length)];
    j7 = items2[rand.nextInt(items2.length)];
    fk = fkVals2[rand.nextInt(fkVals2.length)];
	}
	String sql = "INSERT INTO sf(sid,fk,y1,y2,y3,y4,y5,y6,y7) VALUES ("+i+","+fk+","+j1+","+j2+","+j3+","+j4+","+j5+","+j6+","+j7+")";
	//String query = "SELECT RID, X FROM R";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sql);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	}
	System.out.println("Done");
}
}

