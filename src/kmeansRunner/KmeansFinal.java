package kmeansRunner;

import java.util.*;
import java.sql.*;

class KmeansFinal {
	//declaring number of clusters and number of dimensions in R and S table
	static int clusters=4;
	static int dimr=3;
	static int dims=7;
//This function calculates the sum of the squared difference between a feature and the current mean for that 
//feature.Finally it stores this for each of the clusters in the sf array of the model.Called for each RID.
static void updatesf(int rid,HashMap<Integer,CoordinatesR> rtable,double[][] mu){
	rtable.get(rid).sf = new double[clusters];
    for (int i=0;i<clusters;i++){
    	rtable.get(rid).sf[i]=0;
    	for(int k=0;k<dimr;k++){
    		rtable.get(rid).sf[i]=rtable.get(rid).sf[i]+Math.pow(rtable.get(rid).f.get(k)-mu[i][k],2);	
    	}
	}
}
/*
 * Function does the following:
 * 1. Here we prepare a table that contains SID and the cluster name which is closest to the data point.
 * 2. Each call to this function is made for one SID value.
 * 3. In meanK we keep on adding the distances of each data point from the mean values of clusters(MU). 
 * 4. In order to get the complete features, we need the features from R as well. So we lookup using the 
 *    foreign-key and fetch SF values. SF is an array that contains the sum of squared differences between
 *    features of R and their corresponding cluster means.
 * 5. So now we have the sum of squared differences of the SID point from mean of each cluster - stored
 *    in meanK. We find the minimum value of meanK and it's index. That index corresponds to the cluster id
 *    of the point referenced by the SID. We do it for all the SID points by multiple function calls.The 
 *    resulting table is the intermediate table which contains SID and the corresponding cluster-id.   
 */
static void calmu1mu2(int sid,HashMap<Integer,CoordinatesR> rtable,HashMap<Integer,Integer>a,CoordinatesS c,double[][] mu){
	List<Double> meanK = new ArrayList<Double>();
	for (int i=0;i<clusters;i++){
		double sumDifDim = 0;
		for(int k =dimr;k<dims;k++){
			sumDifDim=sumDifDim+Math.pow(c.f.get(k)-mu[i][k],2);
		}
		meanK.add(rtable.get(c.fk).sf[i]+sumDifDim);
	}
	int minIndex = meanK.indexOf(Collections.min(meanK));
	a.put(sid, minIndex);
}
/*
 * 1. This function calculates the new means for each clusters.
 * 2. We define an array called avg that stores the sum of the coordinates for each SID for each cluster.
 * 3. 'n' is the number of elements in each cluster.To avoid division by zero we initialize it with 1s.
 * 4. We traverse the intermediate table and update the corresponding avg values of clusters by adding the 
 *    features to the respective clusters. Here we add both the R features (through foreign key and use of SID)
 *    and S features (through stable map). Thus we got two for loops.
 * 5. We then take average of each cluster value to get the new means.    
 */
static void calcNewMeans(HashMap<Integer,Integer>intmTable,HashMap<Integer,CoordinatesS> stable,HashMap<Integer,CoordinatesR>rtable,double[][]mu){
	double[][] avg = new double[clusters][dimr+dims];
	int[] n = new int[clusters];
	Arrays.fill(n, 1);
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    for (int i=0;i<dimr;i++){
	    	avg[value][i]=avg[value][i]+rtable.get((stable.get(key).fk)).f.get(i);
	    }
	    for (int i=dimr;i<dims;i++){
	    	avg[value][i]=avg[value][i]+stable.get(key).f.get(i);
	    }
	    n[value]=n[value]+1;
	    for (int l=0;l<clusters;l++){
	    	for(int d=0;d<dimr+dims;d++)
	    	try{	
	    	mu[l][d]=avg[l][d]/n[l];
	    	} catch(Exception e){
	    		e.printStackTrace();
	    	}
	    }    	
	}	
}
//start of main function
public static void main(String args[]) {
	/*
	 * Declaring maps to store R , S and the intermediate table.
	 */
	HashMap<Integer,CoordinatesR> rtable = new HashMap<Integer,CoordinatesR>();
	HashMap<Integer,CoordinatesS> stable = new HashMap<Integer,CoordinatesS>();
	HashMap<Integer,Integer> intmTable = new HashMap<Integer,Integer>();
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	Statement s = null;
	Connection con = null;
	try {
		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
		s = con.createStatement();
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	//declaring the means to store the means of clusters.
	double[][] mu = new double[clusters][dimr+dims];
	ResultSet rs = null;
	//Queries to select values from R and S tables
	String sql = "SELECT rid,x1,x2,x3 FROM rf";
	String sql1 = "SELECT sid,fk,y1,y2,y3,y4,y5,y6,y7 FROM sf";
	//max rid
    int mrid=0;
    //Query execution to fill the maps rtable and stable. Note we store the RID/SID and the 
    //entire coordinate object containing the various features and other attributes.
	try {
		rs = s.executeQuery(sql);
		while(rs.next()) {
			CoordinatesR c= new CoordinatesR();
			c.f = new ArrayList<Double>();
			for (int i=2;i<=dimr+1;i++){
			    c.f.add(Double.parseDouble(rs.getString(i)));
			}
			rtable.put(Integer.parseInt(rs.getString(1)),c);
			if(mrid<Integer.parseInt(rs.getString(1)))
				mrid=Integer.parseInt(rs.getString(1));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	int msid=0;
	try {
		rs = s.executeQuery(sql1);
		while(rs.next()) {
			CoordinatesS c= new CoordinatesS();
			c.f = new ArrayList<Double>();
			for (int i=3;i<=dims+2;i++)
				c.f.add(Double.parseDouble(rs.getString(i)));	
			c.fk = Integer.parseInt(rs.getString(2));
			stable.put(Integer.parseInt(rs.getString(1)),c);
			if(msid<Integer.parseInt(rs.getString(1)))
				msid=Integer.parseInt(rs.getString(1));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
//Start of clustering.
	int tries=0;
//Choose number of iterations here.	
	while(tries<10){
	// call the updatesf that adds the sum of squared differences of mean and features of R table to the 
	// model attribute sf for each cluster.Call for each RID	
	for (int i=1;i<=mrid;i++){
		updatesf(i,rtable,mu);
	}
	//Function to prepare the intermediate Table that will assign a cluster to each SIDs.Call for each SID.
	for (int i =1;i<=msid;i++){
		calmu1mu2(i,rtable,intmTable,stable.get(i),mu);
	}
	//Update the mean values by taking average of the features in R and S for each cluster.
	calcNewMeans(intmTable,stable,rtable,mu);
	tries=tries+1;
	}
	//Print the assgned clusters.
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    System.out.println("key, " + key + " value " + value);
	}
	}
}
