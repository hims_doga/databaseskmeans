package kmeansRunner;

import java.util.*;
import java.sql.*;
import java.util.Random;

class KmeansClusters {
	static int clusters=2;
	static int dim=3;
static void calmu1mu2(int sid,HashMap<Integer,Double>rtable,HashMap<Integer,Integer>a,Coordinates c,double[][] mu){
    //double[] meanK= new double[clusters];
	List<Double> meanK = new ArrayList<Double>();
	for (int i=0;i<clusters;i++){
		meanK.add(Math.sqrt(Math.pow(c.y-mu[i][1],2)+Math.pow(c.z-mu[i][2],2)+Math.pow(rtable.get(c.fk)-mu[i][0],2)));
	}
	int minIndex = meanK.indexOf(Collections.min(meanK));
	a.put(sid, minIndex);
}
static void calcNewMeans(HashMap<Integer,Integer>intmTable,HashMap<Integer,Coordinates> stable,HashMap<Integer,Double>rtable,double[][]mu){
	double[][] avg = new double[clusters][dim];
	int[] n = new int[clusters];
	Arrays.fill(n, 1);
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    avg[value][1]=avg[value][1]+stable.get(key).y;
	    avg[value][2]=avg[value][2]+stable.get(key).y;
	    avg[value][0]=avg[value][0]+rtable.get((stable.get(key).fk));
	    n[value]=n[value]+1;
	    for (int l=0;l<clusters;l++){
	    	for(int d=0;d<dim;d++)
	    	mu[l][d]=avg[l][d]/n[l];
	    }    	
	}	
}
public static void main(String args[]) {
	HashMap<Integer,Double> rtable = new HashMap<Integer,Double>();
	HashMap<Integer,Coordinates> stable = new HashMap<Integer,Coordinates>();
	HashMap<Integer,Integer> intmTable = new HashMap<Integer,Integer>();
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	Statement s = null;
	Connection con = null;
	try {
		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
		s = con.createStatement();
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}	
	double[][] muu1 = new double[clusters][dim];
	ResultSet rs = null;
	String sql = "SELECT rid,x FROM R";
	String sql1 = "SELECT sid,y,z,fk FROM S";

	try {
		rs = s.executeQuery(sql);
		while(rs.next()) {
			rtable.put(Integer.parseInt(rs.getString(1)),Double.parseDouble(rs.getString(2)));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	int msid=0;
	try {
		rs = s.executeQuery(sql1);
		while(rs.next()) {
			Coordinates c= new Coordinates();
			c.y = Double.parseDouble(rs.getString(2));
			c.z = Double.parseDouble(rs.getString(3));
			c.fk = Integer.parseInt(rs.getString(4));
			stable.put(Integer.parseInt(rs.getString(1)),c);
			if(msid<Integer.parseInt(rs.getString(1)))
				msid=Integer.parseInt(rs.getString(1));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	
//(int sid,HashMap<Integer,Double>rtable,HashMap<Integer,Integer>a,Coordinates c,double[] mu1,double[] mu2)	
	//caluclate new means
	int tries=0;
	while(tries<10){
	for (int i =1;i<=msid;i++){
		calmu1mu2(i,rtable,intmTable,stable.get(i),muu1);
	}
//calcNewMeans(HashMap<Integer,Integer>intmTable,HashMap<Integer,Coordinates> stable,HashMap<Integer,Double>rtable,double [] mu1,double [] mu2){
	calcNewMeans(intmTable,stable,rtable,muu1);
	tries=tries+1;
	}
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    System.out.println("key, " + key + " value " + value);
	}
	}
}