package kmeansRunner;


import java.util.*;
import java.sql.*;


/*class Coordinates{
	double y;
	double z;
	int fk;
	double x;
	
}*/


class KmeansRun {
static double mean[];
static double prev_mean[];
static void calmu1mu2(int sid,HashMap<Integer,Double>rtable,HashMap<Integer,Integer>a,Coordinates c,double[] mu1,double[] mu2){
	Double mean1=Math.sqrt(Math.pow(c.y-mu1[1],2)+Math.pow(c.z-mu1[2],2)+Math.pow(rtable.get(c.fk)-mu1[0],2));
	Double mean2=Math.sqrt(Math.pow(c.y-mu2[1],2)+Math.pow(c.z-mu2[2],2)+Math.pow(rtable.get(c.fk)-mu2[0],2));
	if (mean1>mean2)
		a.put(sid, 2);
	else
		a.put(sid, 1);
}
static void calcNewMeans(HashMap<Integer,Integer>intmTable,HashMap<Integer,Coordinates> stable,HashMap<Integer,Double>rtable,double [] mu1,double [] mu2){
	double[] avgm1={0,0,0};
	double[] avgm2={0,0,0};
	int n1=0;
	int n2=0;
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    if (value == 1){
	    	avgm1[1]=avgm1[1]+stable.get(key).y;
	    	avgm1[2]=avgm1[2]+stable.get(key).z;
	    	avgm1[0]=avgm1[0]+rtable.get((stable.get(key).fk));	
	    	n1=n1+1;
	    }
	    else if(value == 2){
	    	avgm2[1]=avgm2[1]+stable.get(key).y;
	    	avgm2[2]=avgm2[2]+stable.get(key).z;
	    	avgm2[0]=avgm2[0]+rtable.get((stable.get(key).fk));	
	    	n2=n2+1;
	    }
	    mu1[0]=avgm1[0]/n1;
	    mu1[1]=avgm1[1]/n1;
	    mu1[2]=avgm1[2]/n1;
	    mu2[0]=avgm2[0]/n2;
	    mu2[1]=avgm2[1]/n2;
	    mu2[2]=avgm2[2]/n2;
	    	
	}
	
}
public static void main(String args[]) {
	HashMap<Integer,Double> rtable = new HashMap<Integer,Double>();
	HashMap<Integer,Coordinates> stable = new HashMap<Integer,Coordinates>();
	HashMap<Double,Double[]> means = new HashMap<Double,Double[]>();
	HashMap<Integer,Integer> intmTable = new HashMap<Integer,Integer>();
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	Statement s = null;
	Connection con = null;
	try {
	
		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
		s = con.createStatement();
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	
	//INSERT INTO public.r(rid, x) VALUES (?, ?);
	//initialize means 
	double[] mu1 = {1.0,5.0,8.0};
	double[] mu2 = {100.0,120.0,124.0};
	ResultSet rs = null;
	String sql = "SELECT rid,x FROM R";
	String sql1 = "SELECT sid,y,z,fk FROM S";
	//String query = "SELECT RID, X FROM R";
	
	try {
		rs = s.executeQuery(sql);
		while(rs.next()) {
			rtable.put(Integer.parseInt(rs.getString(1)),Double.parseDouble(rs.getString(2)));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
	int msid=0;
	try {
		rs = s.executeQuery(sql1);
		while(rs.next()) {
			Coordinates c= new Coordinates();
			c.y = Double.parseDouble(rs.getString(2));
			c.z = Double.parseDouble(rs.getString(3));
			c.fk = Integer.parseInt(rs.getString(4));
			stable.put(Integer.parseInt(rs.getString(1)),c);
			if(msid<Integer.parseInt(rs.getString(1)))
				msid=Integer.parseInt(rs.getString(1));
		}
	} catch(SQLException e) {
		e.printStackTrace();
	}
//(int sid,HashMap<Integer,Double>rtable,HashMap<Integer,Integer>a,Coordinates c,double[] mu1,double[] mu2)	
	//caluclate new means
	int tries=0;
	while(tries<4){
	for (int i =1;i<=msid;i++){
		calmu1mu2(i,rtable,intmTable,stable.get(i),mu1,mu2);
	}
//calcNewMeans(HashMap<Integer,Integer>intmTable,HashMap<Integer,Coordinates> stable,HashMap<Integer,Double>rtable,double [] mu1,double [] mu2){
	calcNewMeans(intmTable,stable,rtable,mu1,mu2);
	tries=tries+1;
	}
	for (HashMap.Entry<Integer, Integer> entry : intmTable.entrySet()) {
	    Integer key = entry.getKey();
	    Integer value = entry.getValue();
	    System.out.println("key, " + key + " value " + value);
	}
	/*try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sql);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	*/
	/*for (HashMap.Entry<Integer, Double> entry : rtable.entrySet()) {
	    Integer key = entry.getKey();
	    Double value = entry.getValue();
	    System.out.println("key, " + key + " value " + value);
	}
	for (HashMap.Entry<Integer, Coordinates> entry : stable.entrySet()) {
	    Integer key = entry.getKey();
	    Coordinates value = entry.getValue();
	    System.out.println("key, " + key + " values " + value.y+","+value.z+","+value.fk);
	}*/
	}
}


