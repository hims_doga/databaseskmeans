# README #

This code base contains the code for running FL and Materialize approach in K-means.
There are following branches for different cases:

1. MemFitLoopsOnly- When data is assumed to fit completely in the memory.
2. MemFitSqlInLoops- CPU costs avoided by pushing all the computations to database.
3. MemNotFitLoopsOnly-When data does not fit completely in memory and Batch processing is required.


For each of the branches there are two files -MkMeans(For materialize) and KMeansFinal(For factorized) Learning.

We generate the data using the file GaussianTablesGenerator.